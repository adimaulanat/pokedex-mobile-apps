import 'react-native'
import React from 'react'
import BackButton from '../../App/Components/BackButton'
import renderer from 'react-test-renderer'
import { Colors } from '../../App/Themes'
import { shallow } from 'enzyme'

test('BackButton component renders correctly if green background is true', () => {
  const tree = renderer.create(<BackButton onPress={() => {}} color={Colors.green} />).toJSON()
  expect(tree).toMatchSnapshot()
})

test('AlertMessage component render white background', () => {
  const tree = renderer.create(<BackButton />).toJSON()
  expect(tree).toMatchSnapshot()
})

test('onPress', () => {
  let i = 0 // i guess i could have used sinon here too... less is more i guess
  const onPress = () => i++
  const wrapperPress = shallow(<BackButton onPress={onPress} />)

  expect(wrapperPress.prop('onPress')).toBe(onPress) // uses the right handler
  expect(i).toBe(0)
  wrapperPress.simulate('press')
  expect(i).toBe(1)
})
