/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the Infinite Red Slack channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import PokemonActions from '../Redux/PokemonRedux'
// import { PokemonSelectors } from '../Redux/PokemonRedux'

export function * getPokemons (api, action) {
  const { data } = action
  const response = yield call(api.getDynamicUrl, data)
  
  if (response.ok) {
    console.log(response.data)
    yield put(PokemonActions.getPokemonsSuccess(response.data))
  } else {
    yield put(PokemonActions.getPokemonsFailure(response.data))
  }
}

export function * getPokemonDetails (api, action) {
  const { data } = action
  const response = yield call(api.getDynamicUrl, data)
  
  if (response.ok) {
    yield put(PokemonActions.getPokemonDetailsSuccess(response.data))
  } else {
    yield put(PokemonActions.getPokemonDetailsFailure(response.data))
  }
}

export function * searchPokemons (api, action) {
  const { data, callback } = action
  const response = yield call(api.searchPokemons, data)
  
  if (response.ok && response.data) {
    yield put(PokemonActions.searchPokemonsSuccess(response.data))
    yield put(PokemonActions.getPokemonDetailsSuccess(response.data))
    if (callback) callback()
  } else {
    yield put(PokemonActions.searchPokemonsFailure())
  }
}

export function * getPokemonsByType (api, action) {
  const { data } = action
  const response = yield call(api.getDynamicUrl, data)
  
  if (response.ok && response.data) {
    yield put(PokemonActions.getPokemonsByTypeSuccess(response.data))
  } else {
    yield put(PokemonActions.getPokemonsByTypeFailure())
  }
}
