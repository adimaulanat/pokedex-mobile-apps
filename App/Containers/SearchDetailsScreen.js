import React, { Component } from 'react';
import {
  Text,
  SafeAreaView,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomHeader } from 'react-native-awesome-component';
import { Colors, Images } from '../Themes';
import BackButton from '../Components/BackButton';
import Scale from '../Transforms/Scale';
import PokemonActions from '../Redux/PokemonRedux';

class SearchDetailScreen extends Component {

  handleLoadMore = () => {
    const { pokemons, fetching } = this.props
    if (fetching) {
      return null
    } else if (pokemons.next) {
      this.props.getPokemons({ api: pokemons.next });
    }
  }

  renderItem = ({ item }) => {
    const { is_filter } = this.props.navigation.state.params
    return (
      <TouchableOpacity
        style={{ height: Scale(50), marginHorizontal: Scale(5), flexDirection: 'row', alignItems: 'center' }}
        onPress={() => {
          if (is_filter) {
            this.props.getPokemonsByType({api: item.url});
            this.props.navigation.push('Filter')
          } else {
            this.props.getPokemonDetails({api: item.url});
            this.props.navigation.push('PokemonDetails')
          }
        }}
      >
        <View>
          <Text style={{ textTransform: 'capitalize' }}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { pokemons } = this.props;
    return (
      <SafeAreaView>
        <CustomHeader
          isCard
          backgroundColor={Colors.transparent}
          renderLeft={() => (
            <BackButton style={{ paddingTop: Scale(50) }} color={Colors.background} onPress={() => this.props.navigation.goBack()} />
          )}
        />
        <Image source={Images.pokeball} style={{ position: 'absolute', right: 0, top: 0 }} />
          <FlatList
            data={pokemons && pokemons.results ? pokemons.results : []}
            renderItem={this.renderItem}
            keyExtractor={item => item.name}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  marginVertical: Scale(5),
                  height: 1,
                  backgroundColor: Colors.orange
                }}
              />
            )}
            contentContainerStyle={{
              paddingHorizontal: Scale(20),
              paddingVertical: Scale(10),
            }}
            extraData={pokemons}
            onEndReached={this.handleLoadMore}
          />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    pokemons: state.pokemon.getPokemons.payload,
    fetching: state.pokemon.getPokemons.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPokemons: data => dispatch(PokemonActions.getPokemonsRequest(data)),
    getPokemonDetails: data => dispatch(PokemonActions.getPokemonDetailsRequest(data)),
    getPokemonsByType: data => dispatch(PokemonActions.getPokemonsByTypeRequest(data)),
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchDetailScreen);
