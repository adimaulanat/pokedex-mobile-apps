import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  StatusBar,
  SafeAreaView,
  View,
  FlatList,
  ActivityIndicator,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomHeader } from 'react-native-awesome-component';
import { Colors, Fonts, Images, Metrics } from '../Themes';
import BackButton from '../Components/BackButton';
import Scale from '../Transforms/Scale';
import * as Progress from 'react-native-progress';

class PokemonDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  renderStats = ({ item }) => {
    return (
      <View style={{ flexDirection: 'row', height: Scale(20), alignItems: 'center' }}>
        <Text style={{ color: Colors.gray, fontWeight: '500', ...Fonts.style.normal, textTransform: 'capitalize', width: Scale(170) }}>
          {item.stat.name}
        </Text>

        <Text style={{ fontWeight: '500', ...Fonts.style.normal, textTransform: 'capitalize', width: Scale(70) }}>
          {item.base_stat}
        </Text>
        <View style={{ height: Scale(6), alignItems: 'center' }}>
          <Progress.Bar
            progress={item.base_stat / 100}
            width={Scale(100)}
            color={item.base_stat <= 50 ? Colors.red : Colors.green}
            borderColor={item.base_stat <= 50 ? Colors.red : Colors.green}
            height={Scale(4)}
          />
        </View>
      </View>
    )
  }

  render() {
    const { payload, fetching } = this.props.pokemon;
    if (fetching) {
      return (
        <ActivityIndicator
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            flex: 1,
          }}
          color={Colors.orange}
        />
      )
    }
    return (
      <SafeAreaView>
        <StatusBar barStyle='dark-content' backgroundColor={Colors.transparent} translucent />
        <CustomHeader
          isCard
          backgroundColor={Colors.transparent}
          renderLeft={() => (
            <BackButton style={{ paddingTop: Scale(50) }} color={Colors.background} onPress={() => this.props.navigation.goBack()} />
          )}
        />
        <Image source={Images.pokeball} style={{ position: 'absolute', right: 0, top: 0 }} />
        <ScrollView style={{ paddingHorizontal: Scale(20) }}>
          <View style={{ flexDirection: 'row', width: Metrics.screenWidth, justifyContent: 'space-between', alignItems: 'flex-start', height: Scale(200) }}>
            <View style={{ width: '50%' }}>
              <Text style={{ fontWeight: '500', ...Fonts.style.h2, textTransform: 'capitalize', marginVertical: Scale(20) }}>
                {payload.name}
              </Text>

              <View style={{ justifyContent: 'flex-start', flexDirection: 'row' }}>
                {payload.types.map(item => (
                  <View style={{ marginRight: Scale(10), paddingHorizontal: Scale(20), paddingVertical: Scale(5), borderRadius: Scale(10), backgroundColor: Colors.orange, alignItems: 'center' }}>
                    <Text style={{ color: Colors.white, fontWeight: '400', ...Fonts.style.normal, textTransform: 'capitalize' }}>
                      {item.type.name}
                    </Text>
                  </View>
                ))}
              </View>
            </View>

            <Image
              source={{ uri: payload.sprites.front_default }}
              resizeMethod="scale"
              resizeMode="cover"
              style={{ width: Scale(180), height: Scale(140), marginRight: Scale(10), alignSelf: 'flex-start' }}
            />
          </View>

          <Text style={{ fontWeight: '500', ...Fonts.style.h5, textTransform: 'capitalize', marginVertical: Scale(20) }}>
            Base Stats
          </Text>

          <FlatList
            data={payload.stats}
            renderItem={this.renderStats}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  marginVertical: Scale(5),
                }}
              />
            )}
            contentContainerStyle={{
              marginTop: Scale(10),
            }}
          />


        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    pokemon: state.pokemon.getPokemonDetails,
  };
};


export default connect(
  mapStateToProps,
  null,
)(PokemonDetailsScreen);
