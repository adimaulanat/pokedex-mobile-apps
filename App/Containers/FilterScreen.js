import React, { Component } from 'react';
import {
  Text,
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomHeader } from 'react-native-awesome-component';
import { Colors, Images, Metrics } from '../Themes';
import BackButton from '../Components/BackButton';
import Scale from '../Transforms/Scale';
import PokemonActions from '../Redux/PokemonRedux';

class FilterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  renderPokemonItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={{ height: Scale(50), marginHorizontal: Scale(5), flexDirection: 'row', alignItems: 'center' }}
        onPress={() => {
          this.props.getPokemonDetails({api: item.pokemon.url})
          this.props.navigation.push('PokemonDetails')
        }}
      >
        <View>
          <Text style={{ textTransform: 'capitalize' }}>{item.pokemon.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { pokemons } = this.props;
    return (
      <SafeAreaView>
        <CustomHeader
          isCard
          backgroundColor={Colors.transparent}
          renderLeft={() => (
            <BackButton style={{ paddingTop: Scale(50) }} color={Colors.background} onPress={() => this.props.navigation.goBack()} />
          )}
        />
        <Image source={Images.pokeball} style={{ position: 'absolute', right: 0, top: 0 }} />
        <View style={{ marginTop: Scale(20) }}>
          <FlatList
            data={pokemons && pokemons.pokemon ? pokemons.pokemon : []}
            renderItem={this.renderPokemonItem}
            keyExtractor={item => item.name}
            ItemSeparatorComponent={() => (
              <View
                style={{
                  marginVertical: Scale(5),
                  height: 1,
                  backgroundColor: Colors.orange
                }}
              />
            )}
            contentContainerStyle={{
              paddingHorizontal: Scale(20),
              paddingVertical: Scale(10),
            }}
            extraData={pokemons}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    pokemons: state.pokemon.getPokemonsByType.payload,
    fetching: state.pokemon.getPokemonsByType.fetching,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPokemonDetails: data => dispatch(PokemonActions.getPokemonDetailsRequest(data))
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FilterScreen);
