import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  StatusBar,
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, Fonts, Images } from '../Themes';
import SearchInput from '../Components/SearchInput';
import Scale from '../Transforms/Scale';
import PokemonActions from '../Redux/PokemonRedux';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
    };
  }

  onSubmitSearching = () => {
    const { searchText } = this.state;
    const { navigation, searchPokemons } = this.props;
    searchPokemons(searchText, () => navigation.push('PokemonDetails'))
  };

  render() {
    return (
      <SafeAreaView>
        <StatusBar barStyle='dark-content' backgroundColor={Colors.transparent} translucent />
        <Image source={Images.pokeball} style={{ position: 'absolute', right: 0, top: 0 }} />
        <ScrollView style={{ paddingHorizontal: Scale(20) }}>

          <View style={styles.baseContainer}>
            <Text style={styles.title}>
              What Pokemon are you looking for?
                </Text>

            <SearchInput
              onChange={text => this.setState({ searchText: text })}
              onFinish={this.onSubmitSearching}
            />

            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.props.getPokemons({ api: 'https://pokeapi.co/api/v2/pokemon?limit=20&offset=0' });
                this.props.navigation.push('SearchDetail', { is_filter: false })
              }}
            >
              <Text style={{ color: Colors.white, ...Fonts.style.normal }}>
                View all pokemons
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.button, { backgroundColor: Colors.orange }]}
              onPress={() => {
                this.props.getPokemons({ api: 'https://pokeapi.co/api/v2/type' });
                this.props.navigation.push('SearchDetail', { is_filter: true })
              }}
            >
              <Text style={{ color: Colors.white, ...Fonts.style.normal }}>
                View pokemons by type
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: { fontWeight: '500', ...Fonts.style.h3, textTransform: 'capitalize', marginVertical: Scale(20), alignSelf: 'center' },
  baseContainer: { alignItems: 'center', flex: 1, alignSelf: 'center', marginTop: Scale(80) },
  button: { width: Scale(250), backgroundColor: Colors.green, height: Scale(40), borderRadius: Scale(30), alignItems: 'center', justifyContent: 'center', marginTop: Scale(30) }
})

const mapDispatchToProps = dispatch => {
  return {
    searchPokemons: (data, callback) => dispatch(PokemonActions.searchPokemonsRequest(data, callback)),
    getPokemons: data => dispatch(PokemonActions.getPokemonsRequest(data)),
  };
};


export default connect(
  null,
  mapDispatchToProps,
)(HomeScreen);
