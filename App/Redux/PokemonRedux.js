import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { DropDownHolder } from '../Components/DropDownHolder'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getPokemonsRequest: ['data'],
  getPokemonsSuccess: ['payload'],
  getPokemonsFailure: ['error'],

  getPokemonDetailsRequest: ['data'],
  getPokemonDetailsSuccess: ['payload'],
  getPokemonDetailsFailure: ['error'],

  searchPokemonsRequest: ['data', 'callback'],
  searchPokemonsSuccess: ['payload'],
  searchPokemonsFailure: ['error'],

  getPokemonsByTypeRequest: ['data'],
  getPokemonsByTypeSuccess: ['payload'],
  getPokemonsByTypeFailure: ['error'],
})

export const PokemonTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const DEFAULT_STATE = {
  data: null,
  fetching: null,
  payload: null,
  error: null
}

export const INITIAL_STATE = Immutable({
  getPokemons: DEFAULT_STATE,
  getPokemonDetails: DEFAULT_STATE,
  searchPokemons: DEFAULT_STATE,
  getPokemonsByType: DEFAULT_STATE,
})

/* ------------- Selectors ------------- */

export const PokemonSelectors = {
  getPokemons: state => state.getPokemons.payload,
  getPokemonDetails: state => state.getPokemonDetails.payload
}

/* ------------- Reducers ------------- */

// request the data from an api
export const getPokemonsRequest = (state, { data }) => {
  return state.merge({ getPokemons: { ...state.getPokemons, fetching: true, data }})
}

// successful api lookup
export const getPokemonsSuccess = (state, { payload }) => {
  const newResults = state.getPokemons.payload && payload.previous ? [...state.getPokemons.payload.results, ...payload.results] : [...payload.results]
  const newData = {...payload, results: newResults }
  return state.merge( { getPokemons: { fetching: false, error: null, payload: newData } })
}

// Something went wrong somewhere.
export const getPokemonsFailure = (state, { error }) => {
  return state.merge({ getPokemons: { fetching: false, error }})
}

export const getPokemonDetailsRequest = (state, { data }) => {
  return state.merge({ getPokemonDetails: { ...state.getPokemonDetails, fetching: true, data }})
}

// successful api lookup
export const getPokemonDetailsSuccess = (state, { payload }) => {
  return state.merge( { getPokemonDetails: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const getPokemonDetailsFailure = (state, { error }) => {
  return state.merge({ getPokemonDetails: { fetching: false, error }})
}

export const searchPokemonsRequest = (state, { data }) => {
  return state.merge({ searchPokemons: { ...state.searchPokemons, fetching: true, data }})
}

// successful api lookup
export const searchPokemonsSuccess = (state, { payload }) => {
  return state.merge( { searchPokemons: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const searchPokemonsFailure = (state) => {
  DropDownHolder.alert(
    'error',
    'Pokemon not found',
    'Please try another keyword',
  );
  return state.merge({ searchPokemons: { fetching: false, error: true }})
}

export const getPokemonsByTypeRequest = (state, { data }) => {
  return state.merge({ getPokemonsByType: { ...state.getPokemonsByType, fetching: true, data }})
}

// successful api lookup
export const getPokemonsByTypeSuccess = (state, { payload }) => {
  return state.merge( { getPokemonsByType: { fetching: false, error: null, payload } })
}

// Something went wrong somewhere.
export const getPokemonsByTypeFailure = (state, { error }) => {
  return state.merge({ getPokemonsByType: { fetching: false, error }})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_POKEMONS_REQUEST]: getPokemonsRequest,
  [Types.GET_POKEMONS_SUCCESS]: getPokemonsSuccess,
  [Types.GET_POKEMONS_FAILURE]: getPokemonsFailure,

  [Types.GET_POKEMON_DETAILS_REQUEST]: getPokemonDetailsRequest,
  [Types.GET_POKEMON_DETAILS_SUCCESS]: getPokemonDetailsSuccess,
  [Types.GET_POKEMON_DETAILS_FAILURE]: getPokemonDetailsFailure,

  [Types.SEARCH_POKEMONS_REQUEST]: searchPokemonsRequest,
  [Types.SEARCH_POKEMONS_SUCCESS]: searchPokemonsSuccess,
  [Types.SEARCH_POKEMONS_FAILURE]: searchPokemonsFailure,

  [Types.GET_POKEMONS_BY_TYPE_REQUEST]: getPokemonsByTypeRequest,
  [Types.GET_POKEMONS_BY_TYPE_SUCCESS]: getPokemonsByTypeSuccess,
  [Types.GET_POKEMONS_BY_TYPE_FAILURE]: getPokemonsByTypeFailure,
})
