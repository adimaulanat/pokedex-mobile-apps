import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import FilterScreen from '../Containers/FilterScreen';
import HomeScreen from '../Containers/HomeScreen';
import PokemonDetailsScreen from '../Containers/PokemonDetailsScreen';
import SearchDetailsScreen from '../Containers/SearchDetailsScreen';

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  SearchDetail: { screen: SearchDetailsScreen },
  PokemonDetails: { screen: PokemonDetailsScreen },
  Filter: { screen: FilterScreen },
  Home: { screen: HomeScreen },
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'Home',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
