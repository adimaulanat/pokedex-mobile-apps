import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import { isEmptyOrSpaces } from 'react-native-awesome-component/src/method/helper';
import { Colors, Fonts } from '../Themes';
import I18n from '../I18n';
import Scale from '../Transforms/Scale';
import colors from '../Themes/Colors';
import images from '../Themes/Images';

const SearchInput = props => {
  const {
    onPress,
    onChange,
    onFinish,
    onClear,
    initialValue = '',
    onPressLogo,
    placeholder = 'Search Pokemon',
  } = props;

  const [text, setText] = useState(initialValue);

  let textInput;

  const clearText = () => {
    if (textInput) {
      setText('');
      if (onClear) onClear();
    }
  };

  return (
    <TouchableOpacity
      style={{
        backgroundColor: Colors.white,
        borderRadius: Scale(6),
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: Scale(14),
        paddingHorizontal: Scale(16),
        // marginTop: isIphoneX() ? Scale(5) : 0,
        paddingRight: 0,
        flex: 1,
        borderWidth: Scale(1),
        borderRadius: Scale(30),
        borderColor: Colors.gray,
        height: Scale(50),
      }}
      activeOpacity={1}
      onPress={onPress}
    >
      <TouchableOpacity onPress={onPressLogo}>
        <Icon name="search" color={Colors.grey} size={Scale(18)} />
      </TouchableOpacity>
      <TextInput
        ref={input => {
          textInput = input;
        }}
        underlineColorAndroid={Colors.transparent}
        placeholder={placeholder}
        value={text}
        pointerEvents={onPress ? 'none' : 'auto'}
        style={{
          flex: 1,
          fontSize: Scale(16),
          // paddingVertical: Scale(10),
          fontFamily: Fonts.type.base,
        }}
        placeholderTextColor={Colors.black}
        editable={!onPress}
        onChangeText={textValue => {
          onChange(textValue);
          setText(textValue);
        }}
        onEndEditing={() => onFinish && onFinish()}
      />
      {!isEmptyOrSpaces(text) && (
        <TouchableOpacity
          style={{ paddingHorizontal: Scale(12), paddingVertical: Scale(8) }}
          onPress={clearText}
        >
          <Icon name="times-circle" size={Scale(20)} color={Colors.gray} />
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

export default SearchInput;
