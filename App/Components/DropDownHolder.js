import I18n from '../I18n';

type AlertType = 'info' | 'warn' | 'error' | 'success';

export type DropdownType = {
  alertWithType: (type: AlertType, title: string, message: string) => void,
};

export class DropDownHolder {
  static dropDown: DropdownType;

  static connectionStatus = true;

  static setDropDown(dropDown: DropdownType) {
    this.dropDown = dropDown;
  }

  static setConnectionStatus(status = true) {
    this.connectionStatus = status;
  }

  static alert(
    type: AlertType,
    title: string,
    message: string,
    forceShow: false,
  ) {
    console.tron.log('on alert ', type, title, message, this.connectionStatus);
    if (this.connectionStatus) {
      this.dropDown.alertWithType(type, title, message);
    }
  }
}

DropDownHolder.defaultProps = {
  connectionStatus: () => true,
};
