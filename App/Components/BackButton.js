import React from 'react'
import { Colors } from '../Themes'
import { TouchableOpacity } from 'react-native'
import Scale from '../Transforms/Scale'
import Icon from 'react-native-vector-icons/MaterialIcons'

const BackButton = props => {
  const { onPress, navigation, color = Colors.white, style } = props
  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <Icon name='arrow-back' size={Scale(28)} color={color} style={{ marginRight: Scale(20) }} />
    </TouchableOpacity>
  )
}

export default BackButton