const colors = {
  background: '#1F0808',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  gray: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  orange: '#fb5f26',
  white: 'white',
  red: '#e73536',
  green: '#4BC07A'
}

export default colors
